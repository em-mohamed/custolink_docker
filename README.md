# Documentation de l'Application React avec Node.js et MySQL dans Docker

## Introduction

Cette documentation décrit les étapes suivies pour créer une application web utilisant React pour le front-end, Node.js pour le back-end, et MySQL pour la base de données. L'ensemble de l'application est conteneurisé à l'aide de Docker, ce qui facilite le déploiement et la gestion de l'environnement de développement.

## Technologies Utilisées

- **Front-end :** React
- **Back-end :** Node.js
- **Base de données :** MySQL
- **Gestion de Conteneurs :** Docker
- **Orchestration de Conteneurs :** Docker Compose

## Structure du Projet

L'application est structurée de la manière suivante :

- projet_docker/
  - back_node/
    - "moncode"
    - Dockerfile
  - front_list/
    - "moncode"
    - Dockerfile
  - init.sql
  - docker-compose.yml



- Le dossier `back_node` contient le code source du back-end Node.js.
- Le dossier `front_list` contient le code source du front-end React.
- Le fichier `init.sql` contient les scripts SQL pour initialiser la base de données.
- Le fichier `docker-compose.yml` définit les services et les dépendances entre les conteneurs.

## Configuration Docker

### Dockerfile pour le Front-end et le Back-end

Les fichiers `Dockerfile` dans les dossiers `front_list` et `back_node` contiennent les instructions pour créer les images Docker respectives pour le front-end et le back-end. Ils sont basés sur des images de base appropriées pour Node.js et React, et ils copient les fichiers source dans les conteneurs.

### Docker Compose

Le fichier `docker-compose.yml` est utilisé pour définir la configuration de l'ensemble de l'application. Il définit trois services : `back_node`, `front_list`, et `db` (base de données).

- Le service `back_node` est construit à partir du `Dockerfile` du back-end et expose le port 5000.
- Le service `front_list` est construit à partir du `Dockerfile` du front-end et expose le port 3000.
- Le service `db` utilise l'image MySQL 5.7. Il définit des variables d'environnement pour le mot de passe root et le nom de la base de données, ainsi qu'un volume pour initialiser la base de données avec le fichier `init.sql`.

### Dépendances entre les Services

- Le service `back_node` dépend du service `db`, ce qui signifie que le back-end ne démarre que lorsque la base de données est prête.
- Le service `front_list` ne dépend pas directement de la base de données, mais il peut communiquer avec le back-end via le réseau Docker.

### Chaîne de Connexion à la Base de Données

Dans le fichier `app.js` du back-end Node.js, la chaîne de connexion à la base de données est configurée pour utiliser le nom d'hôte `db`. Cette configuration fonctionne car le service `db` est défini dans le même réseau Docker que le back-end et le front-end, ce qui permet aux services de se référencer mutuellement par leur nom de service.

## Déploiement

Pour déployer l'application, suivez ces étapes :

1. Assurez-vous d'avoir Docker et Docker Compose installés sur votre machine.

2. Placez votre code source du front-end dans le dossier `front_list` et votre code source du back-end dans le dossier `back_node`.

3. Créez un fichier SQL `init.sql` pour initialiser votre base de données.

4. Exécutez `docker-compose up` depuis le répertoire principal (`projet_docker`) pour construire et démarrer les conteneurs. 
Il est possible que le compose prennent (tres) longtemps à ce finaliser, il bloque aux lignes suivantes : 
* custolink_docker-main-back_node-1   | Express server is running on port 5000
* db_container                        | 2023-09-29T07:20:19.334658Z 2 [Note] Got packets out of order
* db_container                        | 2023-09-29T07:20:20.445736Z 3 [Note] Got packets out of order

5. L'application front-end est accessible à l'adresse `http://localhost:3000`, et le back-end est accessible à `http://localhost:5000`.

## Problèmes Rencontrés et Solutions

Pendant le développement de cette application, plusieurs problèmes ont été rencontrés et résolus. Voici une liste de ces problèmes et des solutions apportées :

### Problème 1 : Docker ne se connecte pas à la base de données MySQL

**Description :** Au début du projet, Docker ne parvenait pas à se connecter à la base de données MySQL. Les conteneurs du back-end Node.js ne pouvaient pas établir de connexion avec le conteneur de la base de données MySQL.

**Solution :** Après avoir examiné la configuration Docker Compose, il a été découvert qu'un autre processus ou service sur l'ordinateur écoutait déjà sur le port 3306, le port par défaut de MySQL. La solution a été d'arrêter ce processus pour libérer le port 3306.

### Problème 2 : Passage à PostgreSQL

**Description :** En raison des problèmes initiaux avec MySQL, une tentative a été faite pour utiliser PostgreSQL à la place. Cependant, lors de la migration vers PostgreSQL, de nouveaux problèmes sont survenus. La base de données PostgreSQL était créée deux fois, et le script SQL d'initialisation (`init.sql`) n'était plus compatible avec PostgreSQL.

**Solution :** Après avoir investigué les problèmes de double création de base de données, il a été découvert que la configuration initiale de PostgreSQL dans le fichier `docker-compose.yml` entraînait la création de la base de données par défaut en plus de celle spécifiée dans `init.sql`. Pour résoudre ce problème, la configuration PostgreSQL a été ajustée pour ne pas créer automatiquement la base de données par défaut.

La compatibilité du script SQL avec PostgreSQL a été résolue en adaptant le script pour qu'il corresponde à la syntaxe SQL de PostgreSQL.

### Problème 3 : Retour à MySQL

**Description :** En fin de compte, il a été décidé de revenir à l'utilisation de MySQL pour la base de données en raison de la familiarité et de la compatibilité avec l'application. Cependant, des problèmes de conflit de port 3306 sont survenus à nouveau.

**Solution :** La solution a été de rechercher le processus ou le service qui écoutait sur le port 3306 et de l'arrêter pour libérer ce port. Une fois le port 3306 libéré, Docker a pu utiliser ce port pour la base de données MySQL sans problème.

Ces problèmes et solutions montrent l'importance de la gestion des ports et des conflits de configuration lors de l'utilisation de conteneurs Docker pour le développement d'applications.

## Conclusion

En suivant ces étapes, vous avez réussi à créer et à déployer une application web complète utilisant React, Node.js et MySQL, tout en utilisant Docker pour la gestion des conteneurs. L'utilisation de Docker Compose simplifie la configuration des dépendances entre les services, ce qui rend le développement et le déploiement de l'application plus efficaces.
