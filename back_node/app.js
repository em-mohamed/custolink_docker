﻿// const express = require('express');
// const cors = require('cors');
// const bodyParser = require('body-parser');
// //const mysql = require('mysql2');
// const usersRouter = require('./routes/users'); // Importez le fichier de route des utilisateurs

// const app = express();
// const port = process.env.PORT || 5000;

// app.use(cors());
// app.use(bodyParser.json());

// // const db = mysql.createConnection({
// //   host: 'db_container',
// //   user: 'root',
// //   password: '241199',
// //   database: 'bdd'
// // });

// // Utilisez la route pour les utilisateurs
// app.use('/api/users', usersRouter);

// app.listen(port, () => {
//     console.log(`Le serveur est en cours d'exécution sur le port ${port}`);
// });


const express = require('express');
const cors = require('cors');
const mysql = require('mysql2'); // Import the mysql2 module
const app = express();

// Create a MySQL connection pool using mysql2
const pool = mysql.createPool({
    host: 'db_container', // Use the IP address of your MySQL container
    port: 3306,
    user: 'root',
    password: '241199',
    database: 'bdd',
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
});

app.use(cors());

// Start the Express server
const port = 5000;
app.listen(port, () => {
    console.log(`Express server is running on port ${port}`);
});

// Récupérer la liste des clients
app.get('/users', (req, res) => {
    const sql = 'SELECT * FROM users';
    // Use the connection pool to handle database queries
    pool.query(sql, (err, result) => {
        if (err) {
            console.error('Error querying the database:', err);
            res.status(500).send('Database error');
            return;
        }
        res.json(result);
    });
});
 

