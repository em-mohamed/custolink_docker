import React from 'react';
import './App.css';
import UserList from './UserList'; // Importez le composant UserList

function App() {
  return (
      <div className="App">
        <UserList /> {/* Utilisez le composant UserList ici */}
      </div>
  );
}

export default App;
