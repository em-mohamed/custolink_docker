import React, { Component } from 'react';
import axios from 'axios';

class UserList extends Component {
    state = {
        users: [],
    };

    componentDidMount() {
        // Effectuez une requête GET vers votre backend
        axios.get('http://localhost:5000/users')
            .then(response => {
                this.setState({ users: response.data });
            })
            .catch(error => {
                console.error('Erreur lors de la récupération des utilisateurs', error);
            });
    }

    render() {
        return (
            <div>
                <h1>Liste des utilisateurs</h1>
                <ul>
                    {this.state.users.map(user => (
                        <li key={user.id}>
                            {user.username} - {user.email}
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}

export default UserList;
