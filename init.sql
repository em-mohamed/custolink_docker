-- Créez la base de données bdd1 si elle n'existe pas déjà
CREATE DATABASE IF NOT EXISTS bdd;

-- Utilisez la base de données bdd1
USE bdd;
-- Créez la table users
CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    phone VARCHAR(20),
    email VARCHAR(255) NOT NULL UNIQUE
);

-- Insérez des données de test (facultatif)
INSERT INTO users (username, password, phone, email)
VALUES
    ('utilisateur1', 'motdepasse1', '1234567890', 'utilisateur1@example.com'),
    ('utilisateur2', 'motdepasse2', '9876543210', 'utilisateur2@example.com');
